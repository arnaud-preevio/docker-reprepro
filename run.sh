#!/bin/bash

set -m

# Configure reprepro

if [ -d "/data/debian" ]
then
    echo "=> /data/debian directory already exists:"
    echo "   So reprepro seems to be already configured, nothing to do..."
else
    echo "=> /data/debian directory does not exist:"
    echo "   Configuring a default debian repository with reprepro..."

    if [ -z "${RPP_DEFAULT_NAME}" ]; then
        echo "=> No repository name supplied: setting to 'Reprepro'"
	RPP_DEFAULT_NAME=Reprepro
    fi

    mkdir -p /data/debian/{tmp,incoming,conf}

    cat << EOF > /data/debian/conf/options
verbose
basedir /data/debian
EOF

    for dist in $(echo ${RPP_DISTRIBUTIONS} | tr ";" "\n"); do
        dcodename_var="RPP_CODENAME_${dist}"
        darchs_var="RPP_ARCHITECTURES_${dist}"
        dcomps_var="RPP_COMPONENTS_${dist}"
        dcodename="${!dcodename_var}"
        if [ -z "${dcodename}" ]; then
            echo "=> No codename supplied for distribution ${dist}: falling back to ${dist} codename"
            dcodename=${dist}
        fi
        cat << EOF >> /data/debian/conf/distributions
Origin: ${RPP_DEFAULT_NAME}
Label: ${RPP_DEFAULT_NAME}
Codename: ${dcodename}
Architectures: ${!darchs_var:-"i386 amd64 armhf source"}
Components: ${!dcomps_var:-"main"}
Description: ${RPP_DEFAULT_NAME} debian repository
EOF
    done

    for incoming in $(echo ${RPP_INCOMINGS} | tr ";" "\n"); do
        iallow_var="RPP_ALLOW_${incoming}"
        mkdir -p /data/debian/incoming/${incoming} /data/debian/tmp/${incoming}
        cat << EOF >> /data/debian/conf/incoming
Name: ${incoming}
IncomingDir: /data/debian/incoming/${incoming}
TempDir: /data/debian/tmp/${incoming}
Allow: ${!iallow_var}
Cleanup: on_deny on_error

EOF
    done
fi

chown -R reprepro:reprepro /data/debian
usermod -d /data/debian reprepro

# Configure HTTP server

echo "=> Configuring HTTP server..."

mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak

cat << EOF > /etc/nginx/sites-available/default
server {
    # Let your repository be the root directory
    root        /data;

    # Always good to log
    access_log  /var/log/nginx/repo.access.log;
    error_log   /var/log/nginx/repo.error.log;

    # Deny everything
    location / {
        deny        all;
        return      404;
    }

    # Allow directories needed for apt
    location ~ /debian/(dists|pool) { }
}
EOF

service nginx start

# Start SSH server

echo "=> Starting SSH server..."

mkdir /run/sshd
exec /usr/sbin/sshd -f /sshd_config -D -e

