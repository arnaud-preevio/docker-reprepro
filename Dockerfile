FROM debian:stretch
MAINTAINER Arnaud Rebillout <arnaud@preev.io>

RUN apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
  gnupg nginx openssh-server reprepro && \
  rm -rf /var/lib/apt/lists/*

# Create a reprepro user (admin)
ARG REPREPRO_UID=600
RUN adduser \
  --system \
  --group \
  --disabled-password \
  --no-create-home \
  --shell /bin/bash \
  --uid $REPREPRO_UID \
  reprepro

COPY sshd_config /sshd_config
COPY run.sh /run.sh

RUN chmod +x /run.sh

VOLUME ["/data"]

EXPOSE 22
EXPOSE 80

CMD ["/run.sh"]
