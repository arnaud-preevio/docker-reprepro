docker-reprepro
===============

Reprepro (Debian packages repository) docker container.

This is a very unsecure reprepro container, intended to be run locally only.

Packages can be uploaded to the repository with `dput` over SSH, and downloaded
using `apt-get` over HTTP. The packages are not signed by reprepro.

Here is a good reference documentation to setup a full reprepro environment:
<http://vincent.bernat.im/en/blog/2014-local-apt-repositories.html>



Building the image
------------------

To create the Docker image `arnaud/reprepro`, execute the following commands.

	git clone https://arnaud-preevio@gitlab.com/arnaud-preevio/docker-reprepro.git
	cd docker-reprepro
	docker build -t arnaud/reprepro .

Additionally, you can specify the `uid` of the reprepro user. This is especially
useful to solve file ownership issues in the shared volume. If you plane to run
this container locally, you might want to do something like that.

	docker build \
	  --tag       arnaud/reprepro \
	  --build-arg REPREPRO_UID=$(id -u) \
	  .



Run the container
-----------------

#### Prepare the data volume

To configure your reprepro container, you need to provide a read-write `/data`
volume, which will be used to write the debian packages reprepro database, and
the reprepro user data (ssh and gpg imported keys).

If the `debian/` directory doesn't already exist in the `/data/` volume,
docker-reprepro will setup a reprepro repository based on configuration
available through environment variables (see `docker run` example below).

If you want to further customize the reprepro configuration, feel free to
provide your own debian reprepro setup in `/data/debian/`.

Let's assume you start from scratch with an empty data volume.

	sudo mkdir -p /var/cache/docker/reprepro

#### Basic command to run the container

Here's a simple example.

	docker run \
	  --name     reprepro \
	  --hostname reprepro \
	  --volume   /var/cache/docker/reprepro:/data \
	  --env RPP_DEFAULT_NAME="Reprepro" \
	  --env RPP_DISTRIBUTIONS=stretch \
	  --env RPP_ARCHITECTURES_stretch="amd64 source" \
	  --env RPP_INCOMINGS="in_stretch" \
	  --env RPP_ALLOW_in_stretch="stretch>stretch" \
	  arnaud/reprepro
	  
And the output should end up like that:

	...
	=> Starting SSH server...
	Server listening on 0.0.0.0 port 22.
	Server listening on :: port 22.

You can as well have a look in the shared volume.

	ls -l /var/cache/docker/reprepro

#### More options for running the container

If you want to have the container reachable at a static IP address, you can use
the `--ip` option. It works only if the container is NOT on the default network.

	# Run on 'my-network' with a static ip address
	--network my-network \
	--ip      172.20.0.10 \

If you want to have the container reachable from outside, you need to publish
some container's ports. You can use `--publish` (or `-p`) for that.

	# Publish ports for http and ssh
	--publish 17222:22 \
	--publish 17280:80 \



Advanced configuration for the container
----------------------------------------

#### Enable SSH connections by adding authorized keys

By default, there's an SSH server up and running, but you need to add authorized
keys if you want to be able to connect.

SSH can be used to upload packages to your reprepro container. For that, you will
want to connect as the `reprepro` user. Reprepro's home directory is located on
the data volume, at the location `/data/debian`. You can easily add an authorized
key with the following snippet.

	mkdir tmp
	cp ~/.ssh/id_rsa.pub tmp/authorized_keys
	sudo chown -R 600:600 tmp
	sudo chmod 644 tmp/authorized_keys
	sudo mv tmp /var/cache/docker/reprepro/debian/.ssh
	
Now you should be able to establish a SSH connection.
	
	ssh reprepro@<container>

#### Enable verification of GPG signed packages

If ever you upload a signed package, reprepro will attempt to validate the signature.
For that to succeed, reprepro needs to know the GPG public key of the maintainer who
signed the package. If it doesn't know about this public key, each time you upload a
package you will be hit by this message.

	Could not check validity of signature ... public key missing!
	
Assuming SSH is up and configured to accept connections, you can send a public key to
the reprepro user with this one-liner

	gpg --export --armor "Maintainer Name" | ssh reprepro@<container> gpg --import

#### Enable GPG signing

TODO



Dput configuration (for packages upload)
----------------------------------------

`dput` is the tool we use to easily upload package into a reprepro container.
Uploading a package to the docker container is done in two steps:
1. we copy the package files to the container.
2. we run a post-upload command to handle to files and update the database.

Here comes some sample configuration for different use cases, to copy-paste
in your `~/.dput.cf`.

#### Local copy

If the container is running on your machine, there's no need to bother with SSH.
Just copy the files locally, and use `docker exec` to run the post upload command.
Of course, this assumes that you have the permissions to write in the shared volume.

```
[reprero-local]
method = local
incoming = /var/cache/docker/reprepro/debian/incoming/in_stretch
allow_unsigned_uploads = 1
allowed_distributions = stretch
post_upload_command = docker exec -u reprepro reprepro bash -c 'reprepro -b /data/debian processincoming in_stretch'
```

#### Scp

If the container is remote, you will need to upload with ssh.

```
[reprepro-remote]
method = scp
ssh_config_options = Port 2222
login = reprepro
fqdn = remotely.com
incoming = /data/debian/incoming/in_stretch
allow_unsigned_uploads = 1
allowed_distributions = stretch
post_upload_command = ssh -p 2222 %(login)s@%(fqdn)s reprepro processincoming in_stretch
```

#### Upload packages

When you're all setup, upload packages with a command such as:

	dput reprepro <some-package>.changes



Apt configuration (for pacakges download)
-----------------------------------------

Add a list file to your apt sources.

	echo 'deb http://reprepro/debian stretch main' \
	> /etc/apt/sources.list.d/reprepro.list

If you didn't configure reprepro to sign the packages, `apt-get` will complain. In this case, you probably want to tell explicitely that this repository is to be trusted.

	echo 'deb [trusted=yes] http://reprepro/debian stretch main' \
	> /etc/apt/sources.list.d/reprepro.list

When this is done, update and have a look at the packages that this repro provides.

	sudo apt-get update && \
	grep ^Package: /var/lib/apt/lists/reprepro_*_Packages 
